import org.junit.Assert;
import org.junit.Test;

public class StackWithPushTimeoutImplTest {

    @Test
    public void gotEmptyStackExceptionTest() {
        StackWithPushTimeoutImpl stackWithPushTimeout = new StackWithPushTimeoutImpl();

        stackWithPushTimeout.push(8);

        Assert.assertEquals(8, stackWithPushTimeout.pop());
        try {
            stackWithPushTimeout.pop();
            Assert.fail("Should got empty stack exception");
        } catch (Exception e) {
            Assert.assertEquals("Empty stack", e.getMessage());
        }
    }

    @Test
    public void checkBasicSplitTest() {
        StackWithPushTimeoutImpl stackWithPushTimeout = new StackWithPushTimeoutImpl();

        stackWithPushTimeout.push(8);
        stackWithPushTimeout.push(3);
        stackWithPushTimeout.push(5);
        stackWithPushTimeout.push(1);
        stackWithPushTimeout.push(0);
        stackWithPushTimeout.push(9);

        Stack returnStack = stackWithPushTimeout.split(o -> o.equals(1));
        Assert.assertEquals(5, returnStack.pop());
        Assert.assertEquals(3, returnStack.pop());
        Assert.assertEquals(8, returnStack.pop());
        Assert.assertEquals(9, stackWithPushTimeout.pop());
        Assert.assertEquals(0, stackWithPushTimeout.pop());
        Assert.assertEquals(1, stackWithPushTimeout.pop());
    }

    @Test
    public void checkSplitWithLongTimeoutTest() {
        StackWithPushTimeoutImpl stackWithPushTimeout = new StackWithPushTimeoutImpl();

        stackWithPushTimeout.push(8);
        stackWithPushTimeout.push(3, 10);
        stackWithPushTimeout.push(5);
        stackWithPushTimeout.push(1);
        stackWithPushTimeout.push(0);
        stackWithPushTimeout.push(9, 10);

        Stack returnStack = stackWithPushTimeout.split(o -> o.equals(1));
        Assert.assertEquals(5, returnStack.pop());
        Assert.assertEquals(8, returnStack.pop());
        Assert.assertEquals(0, stackWithPushTimeout.pop());
        Assert.assertEquals(1, stackWithPushTimeout.pop());
    }
}
