import java.util.function.Predicate;

public interface Stack {

    /**
     * Puts element on the top of the stack right away
     */
    void push(Object element);

    /**
     * Removes and returns element from the top of the stack
     */
    Object pop();

    /**
     * Splits stack into 2 once the condition for particular element is true;
     * Example:
     *   given current stack of ints:
     *     8
     *     3
     *     5
     *     1
     *     0
     *     9
     *   when condition is:
     *     element == 1
     *   than returned stack is:
     *     8
     *     3
     *     5
     *    and current stack is:
     *     1
     *     0
     *     9
     *
     */
    Stack split(Predicate<Object> condition);
}