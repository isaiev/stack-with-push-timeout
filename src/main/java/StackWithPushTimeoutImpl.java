import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;

/**
 * implementation based on bounded array. It is better to rewrite on linked implementation.
 */
public class StackWithPushTimeoutImpl implements StackWithPushTimeout {

    private int size = 0;
    private List<Object> stack;

    // Default capacity for array. One of the limitation of bounded array.
    private static final int DEFAULT_CAPACITY = 1000;
    // FIXME. should be used more than one thread. the number of threads can be configured using the thread pool
    private static final ScheduledExecutorService SCHEDULED_EXECUTOR =
            Executors.newSingleThreadScheduledExecutor();

    public StackWithPushTimeoutImpl() {
        stack = new ArrayList<>(DEFAULT_CAPACITY);
    }

    private StackWithPushTimeoutImpl(int size, List<Object> stack) {
        this.stack = stack;
        this.size = size;
    }

    @Override
    public void push(Object element, int timeout) {
        SCHEDULED_EXECUTOR.schedule(() -> push(element), timeout, TimeUnit.SECONDS);
    }

    @Override
    public synchronized void push(Object element) {
        if (element == null) {
            throw new RuntimeException("Failed to push null value");
        }
        size = size + 1;
        stack.add(element);
    }

    @Override
    public synchronized Object pop() {
        if (size == 0) {
            throw new RuntimeException("Empty stack");
        }
        int pos = size - 1;
        Object popValue = stack.remove(pos);
        size = pos;
        return popValue;
    }

    @Override
    public synchronized Stack split(Predicate<Object> condition) {
        Integer index = findConditionIndex(stack, condition);
        if (index == null) {
            return new StackWithPushTimeoutImpl();
        }
        List<Object> originStack = this.stack;
        List<Object> returnStack = new ArrayList<>(originStack.subList(0, index));

        this.stack = new ArrayList<>(originStack.subList(index, this.size));
        this.size = this.size - index;
        return new StackWithPushTimeoutImpl(index, returnStack);
    }

    private static Integer findConditionIndex(List<Object> stack, Predicate<Object> condition) {
        for (int i = 0; i < stack.size(); i++) {
            if (stack.get(i) == null) {
                return null;
            }
            if (condition.test(stack.get(i))) {
                return i;
            }
        }
        return null;
    }
}
