public interface StackWithPushTimeout extends Stack {

    /**
     * Puts element on the top of the stack only after specified timeout.
     * Method should return right away, however element should be
     * push to the stack only after specified timeout
     *
     * Example:
     * stack.push("cat")
     * stack =>   cat
     *
     * stack.push("dog", 10)
     * stack.push("rabbit")
     * stack.push("pig", 3)
     * stack => cat, rabbit
     *
     * after 3 second
     * stack => cat, rabbit, pig
     *
     * after 7 seconds more
     * stack => cat, rabbit, pig, dog
     *
     * stack.pop() returns "dog"
     *
     *
     * @param element element to push
     * @param timeout timeout in seconds
     */
    void push(Object element, int timeout);
}